require 'erb'

desc 'Load the Battle System environment'
task :environment do
  require 'lib/tbbs.rb'
end

desc 'Loads a console with lib/tbbs'
task :console do
  exec("irb -Ilib -rtbbs")
end

namespace :list do
  desc 'List all of the Status Conditions' 
  task :conditions do
    filenames = formatted_filenames File.join(Dir.pwd, 'lib/tbbs/conditions/*.rb')

    puts ""
    puts 'Status Conditions'
    puts '-----------------------------------------------------------------------'
    puts filenames
    filenames
  end

  desc 'List all Skills'
  task :skills do
    filenames = formatted_filenames File.join(Dir.pwd, 'lib/tbbs/skills/*.rb')
    
    puts ""
    puts 'Skills'
    puts '-----------------------------------------------------------------------'
    puts filenames
    filenames
  end

  def formatted_filenames(path)
    Dir.glob(path).collect { |f| File.basename(f, '.rb').capitalize }.sort.join(', ')
  end
end

namespace :show do
  desc 'Show the implementation of a Status Condition'
  task :condition, :name do |t, args|
    file = File.join(Dir.pwd, "lib/tbbs/conditions/#{args[:name].downcase}.rb")
    if File.exists? file
      code = File.read(file)
      implementation_details "\"#{args[:name].capitalize}\" Status Condition Implementation", code 
    end
  end

  desc 'Show the implementation of a Skill'
  task :skill, :name do |t, args|
    file = File.join(Dir.pwd, "lib/tbbs/skills/#{args[:name].downcase}.rb")
    if File.exists? file
      code = File.read(file)
      implementation_details "\"#{args[:name].capitalize}\" Skill Implementation", code 
    end
  end
end

def implementation_details(heading, details)
  puts ""
  puts heading
  puts '-----------------------------------------------------------------------'
  puts details
  details
end

namespace :generate do
  desc 'generate a new Status Condition'
  task :condition do
    while(true) do
      Rake::Task['list:conditions'].execute
      puts ""
      puts "#####################################################"
      puts "#          Create a New Status Condition            #"
      puts "#                 ctrl-c to quit                    #"
      puts "#####################################################"


      puts ""
      print "1. Status Condition Name: "      
      name = get_name
      break if name.empty?
      puts "#{name}"

      puts ""
      print "2. Status Condition Modifiers: "
      modifiers = get_modifiers

      templ = File.read(File.join(Dir.pwd, "templates/condition.tmpl"))
      templ = ERB.new templ
      code = templ.result(binding)

      file = File.join(Dir.pwd, "lib/tbbs/conditions/#{name.downcase}.rb")
      File.open(file, 'w') { |f| f.write(code) }

      puts ""
      puts "-------------- Created a New Condition --------------"
      puts "-- #{file}"
      puts " "
      puts code
    end
  end

  def get_input
    STDIN.gets.chomp
  end

  def get_name
    while (true)
      name = get_input.strip
      name = name.split.collect { |v| v.capitalize }.join
      if /^[a-zA-Z]+$/ =~ name
        return name
      else
        puts "Invalid name. Please try again."
        print "Name: "
      end
    end
  end

  def get_modifiers
    while (true)
      modifiers = get_input.strip
      modifiers = "{#{modifiers}}" if modifiers[0] != '{' and modifiers[modifiers.size-1] != '}'
      begin
        hash = eval "Hash[#{modifiers}]"
        return hash
      rescue SyntaxError => se
        puts "That's not a valid hash. Try again."
        print "Modifiers: "
      end
    end
  end
end
