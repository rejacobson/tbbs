require 'tbbs/character'

describe Party do
  before(:each) do
    @party = Party.new
  end

  it '#members returns an array of members from the party' do
    @party.should respond_to(:members)
    @party.members.should == []
  end

  describe '#add' do
    before(:each) do
      @char1 = Character.new
      @char2 = Character.new      
    end

    it 'should only add Character objects' do
      @party.add 123
      @party.members.should == []
    end

    it 'adds a character to the party' do
      @party.add @char1
      @party.add @char2
      @party.members.should == [@char1, @char2]
    end

    it 'adds multiple characters at a time' do
      @party.add @char1, @char2
      @party.members.should == [@char1, @char2]
    end
  end
  
end
