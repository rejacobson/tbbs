require 'tbbs/battle'
require 'data'

class FightSkill < Skill
  def self.perform(source, targets)
    modifiers = []
    targets.each do |t|
      modifiers << { :hp => -3, :conditions => ["Sleep"] }
    end
    modifiers
  end
end

describe Battle do
  before(:each) do
    @battle = Battle.new 3
  end

  describe '#initialize' do
    it 'sets the number of rounds' do
      @battle.rounds.should == 3
    end
  end

  describe '#parties' do
    it 'returns the array of parties within the battle' do
      @battle.should respond_to(:parties)
      @battle.parties.should == []
    end
  end

  describe '#add' do
    before(:each) do
      @p1 = Party.new
      @p2 = Party.new
    end

    it 'should only add Party objects' do
      @battle.add 123
      @battle.parties.should == []
    end

    it 'adds a party to the end of the parties list' do
      @battle.add @p1
      @battle.add @p2
      @battle.parties.should == [@p1, @p2]
    end

    it 'adds multiple parties in one call' do
      @p1.add Character.new
      @p2.add Character.new
      @battle.add @p1, @p2
      @battle.parties.should == [@p1, @p2]
    end
  end 

  describe '#has_character?' do
    it 'returns true if the passed in Character is involved in the battle' do
      party = Party.new
      char1 = Character.new
      char2 = Character.new
      party.add char1

      @battle.add party
      @battle.has_character?(char1).should == true
      @battle.has_character?(char2).should == false
    end
  end

  describe '#set_action' do
    before(:each) do
      @noah = Character.load $noah_stats
      @ryan = Character.load $ryan_stats
    end

    it 'adds a Character action event to the action queue, once per round' do
      @battle.set_action @noah, "Attack", @ryan
      @battle.set_action @noah, "Attack", @ryan
      @battle.set_action @ryan, "Attack", @noah

      @battle.action_queue.size.should == 2
      @battle.action_queue.should == [
        {:skill => "Attack", :actor => @noah, :targets => [@ryan]}, 
        {:skill => "Attack", :actor => @ryan, :targets => [@noah]}
      ]
    end

  end

  describe '#sort_queue' do
    it 'sorts the characters in the action queue according to Character :spe (speed); fastest characters at the end of the array' do
      @noah = Character.load $noah_stats
      @heather = Character.load $heather_stats
      @ryan = Character.load $ryan_stats

      @noah.spe = 3
      @heather.spe = 2
      @ryan.spe = 1

      @battle.set_action @noah, "Attack", @ryan
      @battle.set_action @heather, "Attack", @ryan
      @battle.set_action @ryan, "Attack", @ryan

      @battle.sort_queue
              
      @battle.action_queue.should == [
        {:skill => "Attack", :actor => @ryan, :targets => [@ryan]},
        {:skill => "Attack", :actor => @heather, :targets => [@ryan]},
        {:skill => "Attack", :actor => @noah, :targets => [@ryan]}
      ]
    end
  end

  describe 'Running a battle' do
    before(:each) do
      class AttackSkill < Skill
        def self.perform(actor, targets)
          targets.each { |t| t.hp -= actor.dmg }
        end
      end

      @noah = Character.load $noah_stats
      @heather = Character.load $heather_stats
      @ryan = Character.load $ryan_stats

      @noah.spe = 1
      @heather.spe = 2
      @ryan.spe = 3

      @battle.set_action @noah,    :attack, @heather
      @battle.set_action @heather, :attack, @ryan
      @battle.set_action @ryan,    :attack, @noah

      @battle.sort_queue              
    end
  
    describe '#step' do
      it 'pops the action queue' do
        @battle.action_queue.size.should == 3
        @battle.step
        @battle.action_queue.size.should == 2
      end

      it 'returns the action and reactions' do
        @battle.step.should include(:action, :reaction)
      end

      it 'perfoms the action' do
        dmg = @ryan.dmg
        hp = @noah.hp
        @battle.step
        @noah.hp.should == hp - dmg
      end

      it 'returns false when all actions are done' do
        @battle.step.should be_kind_of Hash
        @battle.step.should be_kind_of Hash
        @battle.step.should be_kind_of Hash
        @battle.step.should == false
      end
    end

    describe '#execute_action' do
      before(:each) do
        @action = { :actor => @noah, :skill => "Fight", :targets => [@ryan, @heather] }
      end

      it 'accepts an action and runs it on the characters' do
        @ryan.should_receive(:react).and_return 'Ryan Modifiers'
        @heather.should_receive(:react).and_return 'Heather Modifiers'
        @battle.execute_action(@action).should == ['Ryan Modifiers', 'Heather Modifiers']
      end

      it 'returns a log of the resulting reactions' do
        modifiers = FightSkill.perform(@ryan, [@ryan, @heather])
        @battle.execute_action(@action).should == [ 
          { :actor => @ryan, :modifiers => modifiers[0] }, 
          { :actor => @heather, :modifiers => modifiers[1] }
         ]
      end
    end

    describe '#simulate' do
      it 'quickly steps through an entire round' do
        @battle.action_queue.size.should == 3
        @battle.simulate
        @battle.action_queue.size.should == 0
      end

      it 'returns an array of the actions taken during the round' do
        log = @battle.simulate
        log.should be_kind_of Array
        log.size.should == 3
      end
    end

  end

end
