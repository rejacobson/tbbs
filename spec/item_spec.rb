require 'tbbs/item'

describe Item do
  it '#fetch returns nil if the passed in name was not a string' do
    Item.fetch(1).should == nil
    Item.fetch('').should == nil
    Item.fetch(nil).should == nil
  end

  it '#fetch finds an item using its name' do
    class Item::Weapon::Sword
    end

    Item.fetch("Sword").should == Item::Weapon::Sword
  end
end
