require 'tbbs/condition'
require 'tbbs/item'
require 'tbbs/character'
require 'debugger'
require 'data'

class Item::Weapon::Sword; end
class Item::Weapon::Waffle; end

class Item::Guard::Helmet; end

class Item::Neck::Necklace; end

class Item::Arm::Bracelet; end

class WeakCondition < Condition
  modifiers StatsHash.new({ :str => -3 })
end

class StrongCondition < Condition
  modifiers StatsHash.new({ :str => 3 })
end

class Katana < Item::Weapon::Sword
  include Modifiers
  modifiers Hash[{ :att => 10 }]
end

describe Character do
  before(:each) do
    @char = Character.new
    Character.stub!(:uuid).and_return(rand(0..1000))
  end

  it 'accepts an id as a parameter' do
    char = Character.new 'asdf'
    char.id.should == 'asdf'
  end

  it 'includes the CharacterStats module' do
    Character.new.should respond_to(:stats)
  end

  describe '#load' do
    it 'loads a Character using base stats.  The item, condition and skill modifiers are applied afterwards.' do
      base_stats = { :hp => 1, :mp => 2, :str => 3, :spe => 4, :int => 5, :foc => 6 }
      char = Character.load base_stats
      char.hp.should == 1
      char.mp.should == 2
      char.str.should == 3
      char.spe.should == 4
      char.int.should == 5
      char.foc.should == 6
    end

    it 'loads a Character with a skillset' do
      stats = { :skillset => ["Attack", "Defend", "Fireball"] }
      char = Character.load stats
      char.skillset.should == ["Attack", "Defend", "Fireball"]  
    end

    it 'applies Conditions, Skills and equipped Items after setting the base stats' do
      base_stats = { :hp => 1, :str => 2, :conditions => ["Strong"], :weapon => "Katana" }
      char = Character.load base_stats
      char.hp.should == 1
      char.str.should == 5
      char.conditions.should == ["Strong"]
    end
  end

  describe '#uses' do
    it 'adds a skill, or skills, to the character`s skillset' do
      char = Character.new
      char.uses "Attack"
      char.uses "Spear", "Fireball"
      char.uses ["Sleep", "Defend"]
      char.skillset.should == ["Attack", "Spear", "Fireball", "Sleep", "Defend"]
    end
  end

  describe '#has_skill?' do
    it 'returns true if the Character has the skill' do
      char = Character.new
      char.uses "Attack"
      char.has_skill?("attack").should == true
      char.has_skill?("Defend").should == false
    end
  end

  describe '#react' do
    it 'accepts an actor, a skill and stat modifiers' do
      char = Character.new
      actor = Character.new
      modifiers = {
        :conditions => ["Weak", "Poison"],
        :hp => -5
        }
      
      char.react(actor, "AttackSkill", modifiers).should == { :actor => char, :modifiers => modifiers }
    end
  end

  describe 'Adding modifiers to a Character' do
    before(:each) do
      @noah = Character.load $noah_stats
    end

    it ' #apply! adds stat modifiers to the character stats' do
      modifiers = StatsHash.new({ :str => 2 })
      str = @noah.str

      @noah.apply! modifiers
      @noah.str.should == str + 2
    end

    it ' #apply! converts multipliers into real numbers relative to the base stats' do
      a = StatsHash.new({ :str => 5 })
      b = StatsHash.new({ :str => 'x2' })
      c = StatsHash.new({ :str => '-x2' })
      d = StatsHash.new({ :str => 'x0.5' })
      e = StatsHash.new({ :str => '-x0.5' })
 
      str = @noah.str

      @noah.apply! a
      @noah.str.should == str + 5
      
      @noah.apply! b
      @noah.str.should == (str*2) + 5

      @noah.apply! c
      @noah.str.should == str + 5

      @noah.apply! d
      @noah.str.should == (str*0.5) + 5

      @noah.apply! e
      @noah.str.should == str + 5

      @noah.str = @noah.base_stats[:str]

      @noah.apply! e
      @noah.apply! b

      @noah.str.should == str + (str - (str*0.5)) + ((str*2) - str)
    end

    it ' #apply! applies Status Conditions to the character' do
      str = @noah.str
      modifiers = StatsHash.new({ :str => 2, :conditions => ["Weak"] })

      @noah.apply! modifiers
      @noah.str.should == str - 1
      @noah.conditions.should == ["Weak"]
    end

    describe 'Status Conditions' do
      it ' #afflict adds a Status Condition modifier to the Character stats' do
        str = @noah.str
        @noah.afflict "Weak"
        @noah.str.should == str - 3
        @noah.conditions.should == ["Weak"]
      end

      it ' #afflict removes a Status Condition if the conditions name if prepended with a -' do
        str = @noah.str
        @noah.afflict "Weak"
        @noah.afflict "-Weak"
        @noah.str.should == str
        @noah.conditions.should == []
      end
    
      it ' #afflict removes conflicting status conditions before adding the current one' do
        str = @noah.str
        int = @noah.int

        @noah.afflict "Strong"
        @noah.afflict "Weak"

        @noah.str.should == str - 3
        @noah.int.should == int 
        @noah.conditions.should == ["Weak"]
      end
    end
  end


  it '#slots gets a list of the character weapon, armor, neck and arm slots' do
    char = Character.new
    char.weapon = "Sword"
    char.guard = "Helmet"
    char.neck = "Necklace"

    char.slots.should == ["Sword", "Helmet", "Necklace", ""]
  end

  describe 'Equipment' do
    before(:each) do
      class Item::Weapon::Knife
        include Modifiers
        modifiers StatsHash.new({ :att => 2 })
      end
    end

    it '#equip puts an item into a characters inventory slot' do
      @char.equip :weapon, "Knife"
      @char.equip :arm, "Bracelet"

      @char.slots.should == ["Knife", "", "", "Bracelet"]
    end

    it '#equip returns the item being replaced in the slot' do
      @char.equip :weapon, "Knife"
      @char.equip(:weapon, "Waffle").should == "Knife"
    end

    it '#equip adjusts the character`s stats according to the item modifiers' do
      att = @char.att

      @char.equip :weapon, "Knife"
      @char.att.should == att + 2
    end

    it '#unequip removes the item from the character`s inventory' do
      @char.equip :weapon, "Knife"
      @char.unequip :weapon
      @char.slots.should == ["", "", "", ""]
    end

    it '#unequip removes the item modifiers from the character stats' do
      att = @char.att

      @char.equip :weapon, "Knife"
      @char.unequip :weapon
      @char.att.should == att
    end

    it '#unequip returns the unequipped item name' do
      @char.equip :weapon, "Knife"
      @char.unequip(:weapon).should == "Knife"
    end
   
    it '#reequip removes and re-equips the item' do
      @char.equip :weapon, "Knife"
      att = @char.att
      @char.reequip :weapon
      @char.weapon.should == "Knife"
      @char.att.should == att
    end 
  end

end
