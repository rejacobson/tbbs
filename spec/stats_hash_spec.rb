require 'stats_hash'

describe StatsHash do
  it '#initialize accepts a hash to initialize with' do
    hash = StatsHash.new( { :a => 5, :b => ["asdf"] })
    hash.should == { :a => 5, :b => ["asdf"] }
  end  

  it '#reverse_sign reverses the sign of numeric values in the hash' do
    StatsHash.reverse_sign(5).should == -5
    StatsHash.reverse_sign(-5).should == 5
  end

  it '#reverse_sign adds or removes a prefixed "-" sign from Strings' do
    StatsHash.reverse_sign("Asdf").should == "-Asdf"
    StatsHash.reverse_sign("-Asdf").should == "Asdf"
  end

  it '#reverse_sign reverses each value in an array' do
    StatsHash.reverse_sign([5, "Asdf"]).should == [-5, "-Asdf"]
    StatsHash.reverse_sign([-5, "-Asdf"]).should == [5, "Asdf"]
  end

  it '#reverse_sign prepends or removes a `-` on a multiplier stat (xN)' do
    StatsHash.reverse_sign('x2').should == '-x2'
    StatsHash.reverse_sign('x2.0').should == '-x2.0'
    StatsHash.reverse_sign('x0.5').should == '-x0.5'
  end

  it '#reverse_sign flips boolean (true|false) values' do
    StatsHash.reverse_sign(true).should == false
    StatsHash.reverse_sign(false).should == true
  end

  it '#-@ Overrides the - unary operator to reverse-sign the entire hash' do
    a = StatsHash.new({ :a => 5, :b => "Asdf" })
    (-a).should == { :a => -5, :b => "-Asdf" }
  end

  it '#combine! adds two stats together' do
    a = StatsHash.new({ :a => 5, :b => ["Asdf"] })
    b = StatsHash.new({ :a => 2, :b => ["-Asdf", "Qwerty"] })

    a.combine!(b)
    a.should == { :a => 7, :b => ["Qwerty"] }
  end

  it '#combine! only removes one match from an array' do
    a = StatsHash.new({ :a => ["Asdf", "Asdf", "Qwerty"] })
    b = StatsHash.new({ :a => ["-Asdf", "Qwerty"] })
    a.combine!(b)
    a.should == { :a => ["Asdf", "Qwerty", "Qwerty"] }
  end

  it '#combine! multiplies a stat' do
    a = StatsHash.new({ :a => 4 })
    b = StatsHash.new({ :a => 'x2' })
    c = StatsHash.new({ :a => 'x0.25' })

    a.combine! b
    a.should == { :a => 8 }

    a.combine! c
    a.should == { :a => 2 }
  end

  it '#combine! multiplies the inverse of a -multiplier: `-x2` becomes `x0.5`' do
    a = StatsHash.new({ :a => 10 })
    b = StatsHash.new({ :a => '-x2' })
    c = StatsHash.new({ :a => '-x0.25' })
    
    a.combine! b
    a.should == { :a => 5 }

    a.combine! c
    a.should == { :a => 20 }
  end

  it '#combine! sets a boolean value (true|false)' do
    a = StatsHash.new({ :active => true })
    b = StatsHash.new({ :active => false })
    c = StatsHash.new({ :active => true })
    
    a.combine! b
    a.should == { :active => false }

    a.combine! c
    a.should == { :active => true }

    a.combine! -c
    a.should == { :active => false }
  end
end
