require 'tbbs/condition'

class TestCondition

end

describe Condition do

  it '#fetch gets a Status Condition class by it`s name' do
    Condition.fetch("Test").should == TestCondition
    Condition.fetch("test").should == TestCondition
    Condition.fetch(:Test).should == TestCondition
  end

  it 'Modifiers module is available to sub-classes' do
    class BlueCondition < Condition
      modifiers StatsHash[{ :a => 1, :b => 2 }]
    end

    class RedCondition < Condition
      modifiers StatsHash[{ :c => 3, :d => 4 }]
    end

    BlueCondition.modifiers.should == { :a => 1, :b => 2 }
    RedCondition.modifiers.should ==  { :c => 3, :d => 4 }
  end 

end
