require 'tbbs/skill'

class TestSkill < Skill

end

describe Skill do

  it '#fetch gets a subclassed Skill using the skill name' do
    Skill.fetch("Test").should == TestSkill
    Skill.fetch("test").should == TestSkill
    Skill.fetch(:test).should == TestSkill
  end 

  it '#perform returns false.  The subclass should override this method' do
    Skill.fetch(:test).perform(1, 2).should == false
  end

  it 'sub class should override the #perform method' do
    TestSkill.class_eval do
      def self.perform(source = nil, targets = nil)
        5
      end
    end

    Skill.fetch(:test).perform.should == 5
  end

end

=begin
describe Skill do
  before(:each) do
    @skill = Skill.new "Name", 'eval this string'
  end

  it { @skill.should respond_to(:name) }
  it { @skill.should respond_to(:effect) }

  describe '#initialize' do
    it 'accepts a name as a parameter and lowercases it' do
      attack = Skill.new 'Attack', 'asdf'
      attack.name.should == 'attack'
    end
  end

  describe '#perform' do
    it 'applies the effect using the source and targets' do
      skill = Skill.new "Test", '
        source.str -= 1
        targets[0].str -= 2
      '
      char1 = Character.load :str => 3
      char2 = Character.load :str => 3

      skill.perform char1, [char2]

      char1.str.should == 2
      char2.str.should == 1
    end
  end

  describe '#load' do
    it 'loads a Skill with a hash' do
      hash = { :name => 'Fireball', :effect => 'some effect' }
      skill = Skill.load hash
      skill.name.should == 'fireball'
      skill.effect.should == 'some effect'
    end
  end

end
=end
