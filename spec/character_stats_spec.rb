require 'character_stats'

class TestObject
  include CharacterStats
end

describe 'CharacterStats' do
  it 'allows for stats to be set using, stat :strength, "default"' do
    TestObject.should respond_to(:stat)
  end
end

describe CharacterStats do
  it 'the stats of two different classes including CharacterStats should be mutually exclusive' do
    class One
      include CharacterStats
      stat :one, 1
      stat :ten, 10
    end
    
    class Two
      include CharacterStats
      stat :two, 2
      stat :twenty, 20
    end
  
    One.assignable_stats.should == [:one, :ten]
    Two.assignable_stats.should == [:two, :twenty]

    first = One.new
    second = Two.new

    first.stats.should == { :one => 1, :ten => 10 }
    second.stats.should == { :two => 2, :twenty => 20 }

    first.one = 10
    second.two = 20
    
    first.stats.should == { :one => 10, :ten => 10 }
    second.stats.should == { :two => 20, :twenty => 20 }

    first.stats = { :one => 100, :ten => 1000 }
    second.stats = { :two => 200, :twenty => 2000 }

    first.stats.should == { :one => 100, :ten => 1000 }
    second.stats.should ==  { :two => 200, :twenty => 2000 }

    One.assignable_stats.should == [:one, :ten]
    Two.assignable_stats.should == [:two, :twenty]
  end
end

describe 'CharacterStats', 'An object with CharacterStats' do
  before(:each) do
    @char = TestObject.new
  end

  it '#stats returns all of the live stats' do
    @char.should respond_to(:stats)
    @char.stats.should == {}
  end

  it '#base_stats returns all of the base stats' do
    @char.should respond_to(:base_stats)
    @char.base_stats.should == {}
  end

  it '#stats= sets base stats and live stats using' do
    class Abc
      include CharacterStats
      stat :str, 1
      stat :int, 6
    end
    char = Abc.new

    char.should respond_to(:stats=)
    char.stats = { :str => 5 }
    char.stats.should == { :str => 5, :int => 6 }
    char.base_stats.should == { :str => 5, :int => 6 }
  end

  it 'base stats should persist while live stats can change' do
    class Xyz
      include CharacterStats

      def initialize
        super
      end

      stat :str, 1
      stat :int, 6
    end
    char = Xyz.new

    char.str = 10
    char.int = 11
    char.stats.should == { :str => 10, :int => 11 }
    char.base_stats.should == { :str => 1, :int => 6 }
  end

  it 'creates setters and getters for the stat' do
    TestObject.class_eval do
      stat :strength
    end
    @char = TestObject.new

    @char.should respond_to(:strength)
    @char.should respond_to(:strength=)
    
    @char.strength.should == nil
    @char.strength = 5
    @char.strength.should == 5
  end

  it 'has stats with default values' do
    TestObject.class_eval do
      stat :str, 1
      stat :int, 2
      stat :dex, 3
    end
    @char = TestObject.new

    @char.str.should == 1
    @char.int.should == 2
    @char.dex.should == 3
  end

  it 'has stats that can use a Proc for the default value' do
    TestObject.class_eval do
      stat :hp do mhp end
      stat :mhp, 20
    end
    @char = TestObject.new
   
    @char.hp.should == 20
    @char.hp = 10
    @char.hp.should == 10
  end

  describe '#dstat' do
    it 'has derived stats that use a Proc to calculate the value; no setter' do
      TestObject.class_eval do
        dstat :damage do strength + attack end
        stat :attack, 2
        stat :strength, 5
      end
      @char = TestObject.new
     
      @char.damage.should == 7
      @char.attack.should == 2
      @char.strength.should == 5 
      @char.should_not respond_to(:damage=)
    end
  end

  it 'should dupe default values so that characters don`t share the same default stat object' do
    TestObject.class_eval do
      stat :conditions, []
    end

    char1 = TestObject.new
    char2 = TestObject.new
    
    char1.conditions.object_id.should_not == char2.conditions.object_id
  end


  it '#assignable_stats returns an array of stat names that were set on the class' do
    class Zebra
      include CharacterStats
      stat :a, 1
      stat :b, 2
    end

    class Ox
      include CharacterStats
      stat :x, 10
      stat :y, 20
    end

    Zebra.assignable_stats.should == [:a, :b]
    Ox.assignable_stats.should == [:x, :y]
  end

end
