# Stats setup
$noah_stats = {
  :skillset => [:attack],
  :str => 8,
  :spe => 6,
  :vit => 4,
  :int => 4,
  :foc => 1,
  :luk => 10,
  :attack => 4,
  :parry => 7 
}

$heather_stats = {
  :skillset => [:attack],
  :str => 4,
  :spe => 2,
  :vit => 3,
  :int => 8,
  :foc => 10,
  :luk => 3,
  :attack => 3,
  :parry => 6
}

$ryan_stats = {
  :skillset => [:attack],
  :str => 10,
  :spe => 2,
  :vit => 3,
  :int => 8,
  :foc => 10,
  :luk => 3,
  :attack => 3,
  :parry => 6
}

$bear_stats = {
  :skillset => [:attack],
  :str => 15,
  :spe => 3,
  :vit => 12,
  :int => 2,
  :foc => 1,
  :luk => 1,
  :attack => 9,
  :parry => 1
}

$wolf_stats = {
  :skillset => [:attack],
  :str => 4,
  :spe => 7,
  :vit => 3,
  :int => 4,
  :foc => 10,
  :luk => 3,
  :attack => 4,
  :parry => 1
}

$chicken_stats = {
  :skillset => [:attack],
  :str => 1,
  :spe => 6,
  :vit => 2,
  :int => 1,
  :foc => 1,
  :luk => 5,
  :attack => 3,
  :parry => 1
}

