require 'modifiers'

class TestClass
  include Modifiers
  modifiers Hash[{ :a => 'A' }]
end

describe Modifiers do
  it 'is a helper module for setting up Character stat modifiers' do
    TestClass.modifiers.should == { :a => 'A' }
  end

  it 'adds #on and #off helpers for applying and removing the modifiers from the character' do
    TestClass.should respond_to :on
    TestClass.should respond_to :off 
  end
end
