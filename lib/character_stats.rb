require 'stats_hash'

module CharacterStats
  def self.included(base)
    base.extend ClassMethods
  end

  def initialize(id = nil)
    base_stats
  end

  def stats
    return @_live_stats if @_live_stats
    @_live_stats = {}
    @_live_stats = StatsHash[ *(self.class.assignable_stats.collect { |v| [v, self.send("#{v}")] }.flatten(1)) ]
  end

  def base_stats
    @_base_stats ||= StatsHash[ *(self.class.assignable_stats.collect { |v| [v, self.send("#{v}")] }.flatten(1)) ]
  end

  def stats=(hash)
    hash.each do |name, value|
      next if !self.respond_to? "#{name}="
      base_stats[name] = (value.dup rescue value)
      stats[name]      = (value.dup rescue value)
    end
  end

  module ClassMethods
    def stat(name, default = nil, &block)
      # Getter
      define_method(name) do
        if stats.has_key?(name)
          stats[name]
        else
          value = block.kind_of?(Proc) ? self.instance_eval(&block) : (default.dup rescue default)
          stats[name] = value
        end
      end
      
      # Setter
      define_method("#{name}=") do |value|
        stats[name] = value
      end
      
      assignable_stats.push(name) if !assignable_stats.include? name
    end

    def dstat(name, &block)
      # Getter with block
      if block.kind_of? Proc
        define_method(name) { self.instance_eval &block }
      end
    end

    def assignable_stats
      self.class_variable_set(:@@_assignable_stats, []) if !self.class_variable_defined?(:@@_assignable_stats)
      self.class_variable_get(:@@_assignable_stats)
    end
  end

end
