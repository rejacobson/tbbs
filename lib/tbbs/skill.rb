class Skill
  def self.fetch(name)
    Object.const_get "#{name.to_s.capitalize}Skill"
  end

  def self.perform(source, targets) 
    false
  end
end

# Include all of the Skill classes
Dir[File.dirname(File.expand_path(__FILE__))+'/skills/*'].each do |file|
  require file
end

