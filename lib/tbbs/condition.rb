require 'modifiers'
require 'stats_hash'

class Condition
  include Modifiers

  def self.fetch(name)
    Object.const_get "#{name.to_s.capitalize}Condition"
  end
end


# Include all of the Skill classes
Dir[File.dirname(File.expand_path(__FILE__))+'/conditions/*'].each do |file|
  require file
end
