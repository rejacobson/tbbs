class Item
  TYPES = ['Weapon', 'Guard' ,'Neck', 'Arm']

  class Weapon; end
  class Guard; end
  class Neck; end
  class Arm; end

  def self.fetch(name)
    return nil if !name.kind_of?(String) or name.empty?    

    klass = nil
    TYPES.each do |type|
      begin
        klass = "Item::#{type}::#{name}".split("::").inject(Module) {|acc, val| acc.const_get(val)}
        break
      rescue NameError
        next
      end      
    end
    klass
  end
end

# Include all of the Skill classes
Dir[File.dirname(File.expand_path(__FILE__))+'/items/*'].each do |file|
  require file
end
