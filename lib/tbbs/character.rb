require 'character_stats'

require 'tbbs/item'
require 'tbbs/condition'
require 'tbbs/skill'

class Character
  include CharacterStats

  attr_reader :id

  SLOTS = [ :weapon, :guard, :neck, :arm ]

  def initialize(id = nil)
    super
    @id = id ? id : self.class.uuid
  end

  def self.uuid
    `uuidgen`.strip
  end

  stat :name, ''
  stat :level, 1
  stat :exp, 0
  stat :skillset, []
  stat :conditions, []

  stat :active, true

  # Items
  stat :weapon, '' # weapon
  stat :guard, ''  # armor/guard
  stat :neck, ''   # neck accessory
  stat :arm, ''    # arm accessory

  # Hit Points and Magic Points
  stat :hp do mhp end
  stat :mp do mmp end

  # Character Stats
  stat :str, 1   # strength
  stat :vit, 1   # vitality
  stat :spe, 1   # speed

  stat :int, 1   # intelligence
  stat :foc, 1   # focus

  stat :att, 1   # attack
  stat :par, 1   # parry
  stat :fat, 0   # fatigue

  # Calculated stats
  dstat :dmg do str + att end    # damage
  dstat :def do spe + par end    # defend
  dstat :mhp do vit * str end    # max hp
  dstat :mmp do int * foc end    # max mp

  def self.load(hash)
    _conditions = hash.delete(:conditions)
    _items = Hash[ SLOTS.collect {|s| [s, (hash.delete(s) or "")]} ]

    char = Character.new
    char.stats = hash

    _conditions.each { |c| char.afflict c } if _conditions
    _items.each { |slot, item| char.equip(slot, item) if !item.empty? }

    char
  end

  def slots
    [weapon, guard, neck, arm]
  end

  def apply!(modifiers)
    # Remove and unapply conflicting Conditions
    modifiers.delete(:conditions).each { |c| self.afflict(c) } if modifiers.has_key? :conditions

    # TODO: Unequip changes in items

    # TODO: Apply changes to skills

    # The modifiers should be relative to the Character's unmodified base stats, and then applied as addition here.
    # ie. all multipliers: 'x1.5', should be converted to absolute numeric values.
    # eg.
    #   stats ==      { :str => 8
    #   multiplier == { :str => 'x1.5' }
    #
    #   8 x1.5 == 12
    #   12 - 8 == 5
    #   multiplier == { :str => 5 }   
    array = modifiers.map do |_stat, _v|
      if _v.kind_of?(String)
        _base_value = base_stats[_stat] 
        if _v[0..1] == '-x'
          _v = _v[2..-1].to_f
          _v = _base_value - (_base_value * _v)
        elsif _v[0] == 'x'
          _v = _v[1..-1].to_f
          _v = (_base_value * _v) - _base_value
        end
      end
      [_stat, _v]
    end
    modifiers = StatsHash[array]

    stats.combine! modifiers
  end

  def equip(slot, name)
    slot = slot.to_sym

    # Get the item object
    item = Item.fetch(name)

    # Don't do anything if the item or accessory slot doesn't exist
    return if !SLOTS.include?(slot) or item.nil?

    # Current item in the slot
    current_item = self.send("#{slot}")

    # Assign the new item
    self.send("#{slot}=", name)

    # Apply the item modifiers to the character
    item.on(self) if item.respond_to? :on

    current_item
  end 

  def unequip(slot)
    slot = slot.to_sym

    # Get the current item
    current_item = self.send("#{slot}")

    # Remove the item from the slot
    self.send("#{slot}=", "")

    # Get the 
    item = Item.fetch(current_item)
    
    # Remove the stat modifiers from the character
    item.off(self) if item.respond_to? :off

    current_item
  end

  def reequip(slot)
    self.equip slot, self.unequip(slot)
  end

  def afflict(name)
    # -Condition; remove the condition from the character
    if name[0] == '-'
      name = name[1..name.size]
      status = Condition.fetch name
      conditions.delete name
      status.off(self) if status.respond_to? :off

    # +Condition; add the condition to the player
    else
      status = Condition.fetch name

      # Remove conflicting conditions
      keys = status.modifiers.keys
      keys.delete(:conditions)
      conditions.dup.each do |n|
        s = Condition.fetch n
        if !s.modifiers.select { |k,v| keys.include? k }.empty? 
          conditions.delete n
          s.off(self)
        end
      end

      conditions.push name
      status.on(self) if status.respond_to? :on
    end
  end

  def uses(*skills)
    skills.flatten.each do |skill|
      self.skillset = self.skillset.dup.push(skill.to_s.capitalize)
    end
  end

  def has_skill?(skill)
    skillset.include? skill.to_s.capitalize
  end

  def react(actor, skill, modifiers)
    { :actor => self, :modifiers => modifiers }
  end

end
