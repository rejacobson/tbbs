class AttackSkill < Skill
  def self.perform(source, targets)
    modifiers = []
    targets.each do |t|
      modifiers << { :hp => -source.dmg }
    end
    modifiers
  end
end
