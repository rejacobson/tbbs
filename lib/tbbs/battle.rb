require 'tbbs/skill'
require 'tbbs/party'

class Battle
  attr_reader :parties, :rounds, :action_queue
  
  def initialize(rounds = 3)
    @parties = []
    @rounds = rounds
    @action_queue = []
  end

  def add(*parties)
    parties.each do |p|
      @parties << p if p.kind_of? Party
    end
  end

  def has_character?(char)
    parties.each do |p|
      return true if p.members.include? char
    end
    false
  end

  def set_action(actor, skill, targets)
    return if action_queue.select { |a| a[:actor] == actor }.size > 0
    targets = [targets] if !targets.kind_of? Array
    @action_queue << { :actor => actor, :skill => skill, :targets => targets }
  end

  def step
    return false if @action_queue.empty?
    action = @action_queue.pop
    reaction = execute_action(action)

    { :action => action, :reaction => reaction }
  end

  def execute_action(action)
    skill = Skill.fetch action[:skill]
    modifiers = skill.perform action[:actor], action[:targets]

    reactions = []
    action[:targets].each_with_index do |target, index|
      reactions << target.react(action[:actor], skill, modifiers[index])
    end
    reactions
  end

  def simulate
    log = []

    while result = step do
      log << result
    end

    log
  end

  def sort_queue
    @action_queue.sort! { |a, b| a[:actor].spe <=> b[:actor].spe }
  end 

end
