require 'tbbs/character'

class Party
  attr_reader :members

  def initialize
    @members = []
  end

  def add(*chars)
    chars.each do |c|
      @members << c if c.kind_of? Character
    end
  end
end
