require 'stats_hash'

module Modifiers
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def modifiers(hash = nil)
      if hash
        hash = StatsHash[hash] if !hash.kind_of? StatsHash
        self.class_variable_set :@@_modifiers, hash
      end
      
      self.class_variable_get :@@_modifiers
    end

    def on(char)
      char.apply! modifiers
    end

    def off(char)
      char.apply! -modifiers
    end
  end
end
