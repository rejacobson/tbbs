class StatsHash < Hash
  def initialize(hash = nil)
    self.merge! hash if !hash.nil?
  end

  def -@
    hash = self.dup
    hash.each do |stat, value|
      hash[stat] = StatsHash.reverse_sign value
    end
    hash
  end
  
  def combine!(hash)
    hash.each do |stat, value|
      case self[stat].class.to_s
      when "Array"
        value = [value] if !value.kind_of? Array
        value.flatten.each do |v|
          if v[0] == '-'
            self[stat].delete_if do |val|
              if val == v[1..v.size]
                v = '_'
                true
              end
            end
          else
            self[stat] << v
          end
        end

      when "FalseClass", "TrueClass"
        self[stat] = value

      when "Bignum", "Fixnum", "Float", "Integer", "NilClass"
        self[stat] = 0 if self[stat].nil?
        if value.class.to_s == "String"
          if value[0] == 'x'
            self[stat] *= value[1..value.size].to_f
          elsif value[0..1] == '-x'
            self[stat] *= (1 / value[2..value.size].to_f)
          end
        else
          self[stat] += value
        end

      end

    end
  end

  def self.reverse_sign(value)
    case value.class.to_s
    when "Bignum", "Fixnum", "Float", "Integer"
      -value

    when "FalseClass", "TrueClass"
      !value

    when "String"
      case value[0]
      when '-'
        value[1..value.size]
      else
        "-#{value}"
      end

    when "Array"
      value.collect { |v| StatsHash.reverse_sign(v) }

    end
  end 
end
