$: << File.dirname(File.expand_path(__FILE__)) + '/lib'
$: << File.dirname(File.expand_path(__FILE__))

require 'tbbs/battle'
require 'tbbs/skill'
require 'spec/data'

# Good Guys
party1 = Party.new
  noah    = Character.load $noah_stats
  heather = Character.load $heather_stats
  ryan    = Character.load $ryan_stats

  party1.add noah, heather, ryan

# Bad guys
party2 = Party.new
  bear    = Character.load $bear_stats
  wolf    = Character.load $wolf_stats
  chicken = Character.load $chicken_stats

  party2.add bear, wolf, chicken


# Load the Battle
battle = Battle.new
battle.add party1, party2

battle.set_action noah,    "Attack", bear
battle.set_action heather, "Attack", bear
battle.set_action ryan,    "Attack", wolf

battle.set_action bear,    "Attack", ryan
battle.set_action wolf,    "Attack", noah
battle.set_action chicken, "Attack", heather

#battle.simulate # Quickly simulates the entire round
puts battle.step     # Advances one turn into the round
